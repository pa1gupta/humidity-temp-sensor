import machine, dht, time
from umqtt.robust import MQTTClient

GPIO_DHT22 = 4

CONFIG = {	"broker" : "192.168.86.59",
			"topic_temp" : "home/sensor/temperature",
			"topic_humidity" : "home/sensor/humidity",
			"client_id" : "esp8266_weather",
			"publish_freq" : 10,
			}

class weather:
	temperature = 0
	humidity = 0
	def __init__(self):
		self.sensor = dht.DHT22(machine.Pin(GPIO_DHT22))
		self.mqtt = MQTTClient(	"esp8266_weather", "192.168.86.59")
		self.mqtt.connect()
		print("Connected to {}".format(CONFIG['broker']))
		self.led = machine.Pin(14, machine.Pin.OUT);

	def weather_check(self):
		self.sensor.measure()
		self.temperature = self.sensor.temperature()
		self.humidity = self.sensor.humidity()

		self.mqtt.publish(b'{}/{}'.format(CONFIG['topic_temp'], CONFIG['client_id']), bytes(str(self.temperature), 'utf-8'))

		self.mqtt.publish(b'{}/{}'.format(CONFIG['topic_humidity'], CONFIG['client_id']), bytes(str(self.humidity), 'utf-8'))

	def toggle_led(self):
		self.led.value(not self.led.value())

w = weather()

while True:
	w.weather_check()
	w.toggle_led()
	print("Published temp " + str(w.temperature) + " humidity " + str(w.humidity))
	time.sleep(CONFIG["publish_freq"])
